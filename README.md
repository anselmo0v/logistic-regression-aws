# Machine Learning Engineering Assignment

This is the code for a proposed solution that uses AWS Lambda Functions to deploy the given model.

As it can be seen in the Dockerfile, the base image for the solution is the python 3.10 image provided by AWS that is specifically optimized for Lambda functions implementations.

This requires the code in main.py to be contained inside a handler() function that is exposed as entrypoint to the Lambda function once deployed.

The .yml file contains the code for the pipeline that:

    1. Retreives credentials from the AWS account.
    
    2. Builds the docker image from the Dockerfile.

    2. Pushes the created image to the correspondent AWS ECR repository.

    3. Updates the correspondent AWS Lambda function with the new image version.
